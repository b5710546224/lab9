import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/** 
 * this is main class
 */
public class main {
	public static void main(String[]args) throws IOException{
		WordCounter counter = new WordCounter();
		System.out.println(counter.countSyllables("goodbye"));
		
		String FILE_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = new URL(FILE_URL);
		InputStream input = url.openStream();
		System.out.println(counter.countWords(input));
		System.out.print(counter.totalSyl);
	}
}


public interface State {
	public void handleChar(char c);
	public void enterState();
}
